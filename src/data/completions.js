const completions = [
	`להפליץ בקול ולהישאר עם פרצוף רציני`,
	`לפתוח חשבון אינסטגרם פיקטיבי ולעקוב אחרי חיילים`,
	`לגנוח בזמן שמעלים גרסה`,
	`לגנוב לאנשים חלב מהמקרר`,
	'לצעוק על אנשים זרים',
	'ללשתות מים מהאסלה',
	'לגנוב לשייקה פסטרמה',
	'לגור בדירה שכורה ולהמשיך לבקש מאמא שתבשל',
	'לשלוח הודעות לאמהות של חיילים',
	'לחטט באף ולמרוח מתחת לשולחן',
	'לצוד אשכנזים',
	'להתניע את תרץ 3 בווב',
	'לכתוב מיון אינדקסים עם אצבע קטועה של חניך',
	'לשטוף ידיים פחות מ20 שניות',
	'לנגב את התחת ולהריח את התוצרים',
	'למרוח קורונה על מזוזות',
	'ללכת ברחוב ולהשתעל ליד עוברי אורח',
	'לגדל זקן כדי לפצות על איבר מיותר במערך',
	'למחוק את הפייסבוק ולהשתמש רק בתיק חניך',
	'לקנות דג לחדס',
	'לשיר את המנון מונקו במקום את התקווה במסדר רמד',
	'לנהוג בשכרות ולדרוס את יאיר נתניהו',
	`לעקוץ מקסות בידיעה שהולכת להיות מגפה עולמית`,
	`לתרום כליה בתמורה למתאם hdmi`,
	`ללקק אסלה ולקבל קרנולה`,
	`לנשק נסיך ולהפוך לצפרדע`,
	`לבדוק סורס של חניך שהודח`,
	`להדיח חניך כדי להשתמש בשוחר שלו בחד"א`,
	`לקנות עם חניך 2 פחיות ב5`,
	`לעשות קוק לפני תרץ`,
	`להחליף לחניכים את העכברים באוזניים של עזים`,
	`לקנות שעון של רולקס ולבדוק את השעה בטלפון`,
	`לעשות על אח שלך שיחת נזיפה לשם האימון`,
	`לעשות אתר פורנו כמגמה אישית כדי שלצפות בזה יחשב כדיבאג`,
	`לקנות כרוב לישיבת סגל`,
	`לשלוח לרס"ר הודעות בעת שכרות`,
	`להפוך את 'יום גשום כבר סוף עונה' לשיר הסגלי`,
	`לרשום על חלב פאבליק את השם`,
	`להבין את ההבדל שבין eval לsteval`,
	`לזכור את השמות של כל החניכים`,
	`לעשות עם חניך שיחה בהליכה יד ביד`,
	`לעשות בייסיסיטר לילדים של יהודה`,
	`לנשק חניך במקום מזוזה`,
	`להוריד לחניך סטירה`,
	`להוציא את מיצי מהמדור`,
	`לרגש את דנציגל`,
	`למשב סקירת חדשות בגרעפסים`,
	`לשמור כשרות`,
	`לדרג את החניכים בקורס לפי מראה חיצוני`,
	`לפרוץ לבית של אביחי ולשתות לו מהמים`,
	`ליצור בפייסבוק עמוד השפלה לחניך`,
	`להמשיך לתכנת בCPP גם בשלב ב'`,
	`לנהל רומן עם עובד מהחד"א רק בשביל עוד מנה`,
	`לגלות סרטי פורנו בכונן אישי של חניך ולבקש לנזוף בו רק כדי לגלות מאיפה הוא מוריד`,
	`להצליף בחניך ולדרוש ממנו לצעוק שוויל זה הדבר הכי חשוב בקורס תכנות`,
	`לשחק במור"ק מחבל ולרצוח את כל החניכים על אמת`,
	`להשתין לבקבוק ולהביא לישיבת סגל בתור מיץ תפוחים`,
	`ללכת לבני ברק בטיול סוף`,
	`להגיד לחניכים לחפש את החידה המתחבאת ולהחביא תמונה של בולבול ענק`,
	'לגלות שניר קופ מאומת',
	'לגנוב לאודו מהטחינה',
	'להכניס גז מדמיע למאווררים בשקד',
	'להרשם לקורס במרשל כדי לבכות שש שעות בשירותים ושאף אחד לא ידע',
	'להפוך את זוט עני לצלצול בפלאפון, שעון מעורר והתראה להקשב',
	'לעשות ויסית לזיכוי תורנות',
	'להשתעל לחניך על הכומתה ואז להיכנס לבידוד',
	'להרעיל את החניכים על יום ספורט שלא יצא לפועל',
	'להגיד לכל הקורס לעשות ערימת ילדים ענקית כדי לתרגל עבודת צוות',
	'להביא פריסה מהסמלתו',
	'ללכת לטיפול זוגי עם פנטנש',
	'להתבלבל בין מאפינס לתינוק',
	'לתת לינון לנאום באום',
	'לברך את בלאס על התספורת החדשה',
	'לתפור את הברך של חניך אחד לתנוך של חניך אחר',
	'להבין שמפטוטה היא בעצם קריאה למצוקה עקב מצב נפשי לא יציב',
	'לשרוף את סיז ולצפות בו בוער',
	'להבין מי זה עומרי רצון',
	'לשיר "אוהבים אותך שלו שושי"',
	'לתת לאהוד סטירה כל פעם שנגמר סורס',
	'להתארח אצל עמית בארוחת שישי',
	'לראות ריק ומורטי עם ניר (פאקינג) קופ',
	'לתקוע גרעפס בתוך המסיכה',
	'להגיד מפטוטה',
	'ללמוד להשתמש בPortainer',
	'להסתכל על שעון יד כמו אוכל',
	'לקנות חולצה חדשה לנעם קרמר',
];

export default completions;
